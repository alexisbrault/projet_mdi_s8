package invoker;

public interface UI {

	public String getText();

	public int getStartPosSelection();

	public int getEndPosSelection();

	public int getTextLength();
}
