package invoker;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import command.Command;
import command.CommandName;

@SuppressWarnings("serial")
public class UIImpl extends JFrame implements UI {

	private HashMap<CommandName, Command> commands; //Liste des commandes enregistrees (possibles)
	private String text; //Texte courant dans l'editeur
	private int debutSelection;
	private int finSelection;
	
	JPanel container = new JPanel(new BorderLayout());
    JPanel north = new JPanel(new BorderLayout());
	JPanel menu = new JPanel(new FlowLayout());
	JPanel enreg = new JPanel(new FlowLayout());
	
	JButton copier = new JButton(new ImageIcon(ImageIO.read(new File("./../images/copy.png"))));
	JButton couper = new JButton(new ImageIcon(ImageIO.read(new File("./../images/cut.png"))));
	JButton coller = new JButton(new ImageIcon(ImageIO.read(new File("./../images/paste.png"))));
	
	JTextArea txtArea = new JTextArea();

	public UIImpl(HashMap<CommandName, Command> commands) throws IOException{
		super("Editeur de texte");
		this.commands = commands;

		/* Parametres de la fenetre principale */
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);


		text = new String();
		debutSelection = 0;
		finSelection = 0;

		/* Creation des ActionListener pour les differents boutons */
		copier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exeCmd(CommandName.COPY);

				// Gestion visuelle de l'IHM
				txtArea.copy();
			}

		});

		coller.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exeCmd(CommandName.PASTE);

				// Gestion visuelle de l'IHM
				txtArea.paste();

				text = txtArea.getText();
				exeCmd(CommandName.INSERT);
			}

		});

		couper.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exeCmd(CommandName.CUT);

				// Gestion visuelle de l'IHM
				txtArea.cut();
			}

		});

		/* Ajout des boutons � l'interface */
		menu.add(copier);
		menu.add(couper);
		menu.add(coller);

		/* Ajout des listeners sur le JTextArea */
		txtArea.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				debutSelection= e.getDot();
				finSelection = e.getMark();
				//System.out.println("SELECTING "+startPosSelection+" - "+endPosSelection);
				exeCmd(CommandName.SELECT);
			}
		});

		txtArea.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {
				text = txtArea.getText();
				exeCmd(CommandName.INSERT);
			}

			@Override
			public void keyTyped(KeyEvent e) {

			}

		});

		/* Ajout d'une barre de d�filement sur le JTextArea */
		JScrollPane scrolltxt = new JScrollPane(txtArea); 
		txtArea.setEditable(true);

		/* Disposition des differents panels de la fenetre */
		north.add(menu, BorderLayout.WEST);
		container.add(north, BorderLayout.NORTH);
		container.add(scrolltxt, BorderLayout.CENTER);
		this.setContentPane(container);

		/* Affichage de la fenetre */
		setVisible(true);

	}

	public void setCommands(HashMap<CommandName, Command> commandes) {
		this.commands = commandes;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public int getStartPosSelection() {
		return debutSelection;
	}

	@Override
	public int getEndPosSelection() {
		return finSelection;
	}

	@Override
	public int getTextLength(){
		return text.length();
	}

	/**
	 * Execute la commande passee en parametre
	 * @param cmd
	 */
	public void exeCmd(CommandName cmd) {
		commands.get(cmd).execute();
	}
}
