package receiver;

public class Clipboard {
	private final int BASE_CAPACITY = 500;
	
	private StringBuffer value;
	
	public Clipboard() {
		value = new StringBuffer(BASE_CAPACITY);
	}
	
	/**
	 * Lit l'integralite du presse papier
	 * @return Une chaine de caracteres possedant le contenu du presse papier
	 */
	public String read() { 
		return value.toString();
	}
	
	/**
	 * Remplace l'integralite du presse papier par un autre contenu
	 * @param text Un texte qui remplace le contenu du presse papier deja existant
	 */
	public void write(String text) {
		clear();
		value.append(text);
	}
	
	public void clear() {
		value.setLength(0);
	}
}
