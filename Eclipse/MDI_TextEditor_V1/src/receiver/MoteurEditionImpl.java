package receiver;

public class MoteurEditionImpl implements MoteurEdition {

	private Buffer buffer;
	private Selection selection;
	private Clipboard clipboard;
	
	public MoteurEditionImpl() {
		this.buffer = new Buffer();
		this.selection = new Selection(0, 0);
		this.clipboard = new Clipboard();
	}

	@Override
	public void cut() {
		/* Ecrire dans le presse papier le contenu selectionne en supprimant ce dernier du buffer*/
		clipboard.write(buffer.read(selection.getStartPosition(), selection.getEndPosition(), true));
	}

	@Override
	public void copy() {
		/* Ecrire dans le presse papier le contenu selectionne */
		clipboard.write(buffer.read(selection.getStartPosition(), selection.getEndPosition(), false));
	}

	@Override
	public void paste() {
		/* Ecrire dans le buffer le contenu du presse papier */
		buffer.write(selection.getStartPosition(), selection.getEndPosition(), clipboard.read());
	}

	@Override
	public void insert(String s) {
		/* Insertion dans le buffer */
		if(selection.getStartPosition() == selection.getEndPosition())
			buffer.write(selection.getStartPosition(), s);
		else
			buffer.write(selection.getStartPosition(), selection.getEndPosition(), s);
	}

	@Override
	public void changeSelection(int start, int end) {
		selection.setPositions(start, end);
	}
}
