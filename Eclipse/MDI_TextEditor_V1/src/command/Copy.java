package command;

import receiver.MoteurEdition;

/**
 * Copy the selected text
 *
 */
public class Copy implements Command {
	
	private MoteurEdition moteur;
	
	public Copy(MoteurEdition m) {
		this.moteur = m;
	}
	
	@Override
	public void execute() {
		moteur.copy();
	}
}
