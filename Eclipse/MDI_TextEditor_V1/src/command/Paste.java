package command;

import receiver.MoteurEdition;

/**
 * Paste the copied text
 *
 */
public class Paste implements Command {
	
	private MoteurEdition moteur;
	
	public Paste(MoteurEdition m) {
		moteur = m;
	}
	
	@Override
	public void execute() {
		moteur.paste();
	}

}
