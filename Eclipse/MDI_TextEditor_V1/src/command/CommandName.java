package command;

public enum CommandName {
	CUT, PASTE, COPY, INSERT, SELECT
}
