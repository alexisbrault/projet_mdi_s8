package command;

import invoker.UI;
import receiver.MoteurEdition;

/**
 * Selection with the cursor
 *
 */
public class ChangeSelection implements Command {

	private MoteurEdition motor;
	private UI ui;
	
	public ChangeSelection(MoteurEdition m, UI ihm) {
		this.motor = m;
		this.ui = ihm;
	}
	
	@Override
	public void execute() {
		motor.changeSelection(ui.getStartPosSelection(), ui.getEndPosSelection());
	}

}
