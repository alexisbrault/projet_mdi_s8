package command;

/**
 * Command interface
 *
 */

public interface Command {
	/**
	 * Execute the command
	 */
	public void execute();
}
