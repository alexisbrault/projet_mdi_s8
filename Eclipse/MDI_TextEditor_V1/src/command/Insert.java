package command;

import invoker.UI;
import receiver.MoteurEdition;

/**
 * Insert text
 *
 */
public class Insert implements Command {
	
	private MoteurEdition motor;
	private UI ui;
	
	public Insert(MoteurEdition m, UI ui) {
		this.motor = m;
		this.ui = ui;
	}
	
	@Override
	public void execute() {
		motor.insert(ui.getText());
	}

}
