package command;

import receiver.MoteurEdition;

/**
 * Cut the selected text 
 *
 */
public class Cut implements Command {
	
	private MoteurEdition moteur;
	
	public Cut(MoteurEdition m) {
		this.moteur = m;
	}
	
	@Override
	public void execute() {
		moteur.cut();
	}

}
