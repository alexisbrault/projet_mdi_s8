package client;

import java.io.IOException;
import java.util.HashMap;

import command.ChangeSelection;
import command.Command;
import command.CommandName;
import command.Copy;
import command.Cut;
import command.Insert;
import command.Paste;
import invoker.UIImpl;
import receiver.MoteurEdition;
import receiver.MoteurEditionImpl;

/**
 * Class contain the main
 * Client part with command pattern
 *
 */
public class Configurator {
	

	private static UIImpl ui;
	private static MoteurEdition motor;
	
	/**
	 * Instantiate tools editor
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		/* Insertion des commandes dans la liste des commandes */
		HashMap<CommandName, Command> commands = new HashMap<CommandName, Command>();
		
		motor = new MoteurEditionImpl();
		ui = new UIImpl(commands);
		
		commands.put(CommandName.CUT, new Cut(motor));
		commands.put(CommandName.PASTE, new Paste(motor));
		commands.put(CommandName.COPY, new Copy(motor));
		commands.put(CommandName.INSERT, new Insert(motor, ui));
		commands.put(CommandName.SELECT, new ChangeSelection(motor, ui));
		
		
	}
	
}
