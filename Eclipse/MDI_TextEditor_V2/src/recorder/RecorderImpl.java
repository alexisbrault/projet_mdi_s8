package recorder;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import receiver.MoteurEdition;

public class RecorderImpl implements Recorder{

	private MoteurEdition motor;
	private List<MementoCommand> records;
	
	private boolean isReplaying = false;
	
	public RecorderImpl(MoteurEdition motor){
		this.motor = motor;
		this.records = new LinkedList<MementoCommand>();
	}
	
	@Override
	public void record() {
		records.clear();
		motor.setRecording(true);
	}

	@Override
	public void stop() {
		motor.setRecording(false);
	}

	@Override
	public void replay() {
		setReplaying(true);
		Iterator<MementoCommand> it = records.iterator();
		while (it.hasNext()) {
			MementoCommand cm = it.next();
			
			cm.getCommand().setMemento(cm.getMemento());
			cm.getCommand().execute();
		}
		
		setReplaying(false);
	}

	@Override
	public void reset() {
		records.clear();
	}

	public boolean isReplaying() {
		return isReplaying;
	}

	public void setReplaying(boolean isReplaying) {
		this.isReplaying = isReplaying;
	}

	@Override
	public void addCommandMemento(MementoCommand cm) {
		records.add(cm);
	}

}
