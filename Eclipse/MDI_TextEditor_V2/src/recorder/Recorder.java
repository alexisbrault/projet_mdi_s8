package recorder;


/**
 * Interface used to control the user record
 */
public interface Recorder {

	/**
	 * Start the record of the user commands
	 */
	public void record();

	/**
	 * Stop the record of the user commands
	 */
	public void stop();
	
	/**
	 * Replay the saved user commands
	 */
	public void replay();

	/**
	 * Add an sub-class mememto object
	 * @param mc : object with the command and it memento
	 */
	public void addCommandMemento(MementoCommand mc);
	
	/**
	 * Clear the saved user command list
	 */
	public void reset();

	public boolean isReplaying();
	
	public void setReplaying(boolean rejouerBool);
}