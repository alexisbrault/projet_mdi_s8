package recorder;

import command.Command;
import memento.Memento;

public class MementoCommand {
	
	private Command command;
	private Memento memento;
	
	public MementoCommand(Command c, Memento m){
		this.command = c;
		this.memento = m;
	}

	public Command getCommand() {
		return command;
	}

	public Memento getMemento() {
		return memento;
	}
	
}
