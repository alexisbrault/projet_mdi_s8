package invoker;

public interface UI {

	public String getText();

	public void setText(String text);

	public String getLastChar();

	public int getStartPosSelection();

	public int getEndPosSelection();

	public int getTextLength();
	
	public void enableUndo(boolean bool);

	public void enableRedo(boolean bool);
}
