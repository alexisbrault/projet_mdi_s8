package invoker;


import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import command.Command;
import command.CommandName;

@SuppressWarnings("serial")
public class UIImpl extends JFrame implements UI {

	private HashMap<CommandName, Command> commands; //Liste des commandes enregistrees (possibles)
	private String text; //Texte courant dans l'editeur
	private char lastChar;
	private int debutSelection;
	private int finSelection;
	
	JPanel container = new JPanel(new BorderLayout());
    JPanel north = new JPanel(new BorderLayout());
	JPanel menu = new JPanel();
	JPanel enreg = new JPanel();
	JPanel redoUndo = new JPanel();
	
	JButton copier = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/copy.png"))), 40, 40));
	JButton couper = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/cut.png"))), 40, 40));
	JButton coller = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/paste.png"))), 40, 40));
	
	JLabel enregLabel = new JLabel("Macro :");
	JButton demarrer = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/record.png"))), 40, 40));
	JButton stopper = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/stop.png"))), 40, 40));
	JButton rejouer = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/play.png"))), 40, 40));
	
	JButton defaire = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/undo.png"))), 40, 40));
	JButton refaire = new JButton(resize(new ImageIcon(ImageIO.read(new File("./../images/redo.png"))), 40, 40));
	
	JTextArea txtArea = new JTextArea();

	public UIImpl(HashMap<CommandName, Command> commands) throws IOException{
		super("Editeur de texte");
		this.commands = commands;

		/* Parametres de la fenetre principale */
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
		text = new String();
		debutSelection = 0;
		finSelection = 0;
		defaire.setEnabled(false);
		refaire.setEnabled(false);
		
		/* Creation des ActionListener pour les differents boutons */
		copier.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exeCmd(CommandName.COPY);
				
				// Gestion visuelle de l'IHM
				txtArea.copy();
			}
		});
		
		coller.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exeCmd(CommandName.PASTE);
				
				// Gestion visuelle de l'IHM
				txtArea.paste();
				
				text = txtArea.getText();
				exeCmd(CommandName.INSERT);
				
				// Ajout de la sauvegarde du l'�tat actuel pour defaire/refaire
				text = txtArea.getText();
				exeCmd(CommandName.ADDBACKUP);
			}
			
		});
		
		couper.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				exeCmd(CommandName.CUT);
				
				// Gestion visuelle de l'IHM
				txtArea.cut();
				
				text = txtArea.getText();
				exeCmd(CommandName.ADDBACKUP);
			}
			
		});
		
		demarrer.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				exeCmd(CommandName.START);
				enableRecord(false);
				enableStop(true);
			}
		});
		
		stopper.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				exeCmd(CommandName.STOP);
				enableRecord(true);
				enableStop(false);
			}
		});
		
		rejouer.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				exeCmd(CommandName.REPLAY);
			}
		});
		
		defaire.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				exeCmd(CommandName.UNDO);
			}
			
		});
		
		refaire.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				exeCmd(CommandName.REDO);
			}
			
		});
		
		/* Ajout de Tooltips aux boutons */
		
		copier.setToolTipText("Copier");
		couper.setToolTipText("Couper");
		coller.setToolTipText("Coller");
		demarrer.setToolTipText("Demarrer");
		stopper.setToolTipText("Stop");
		rejouer.setToolTipText("Rejouer");
		defaire.setToolTipText("D�faire");
		refaire.setToolTipText("Refaire");
		
		/* Ajout des boutons � l'interface */
		menu.add(copier);
		menu.add(couper);
		menu.add(coller);
		
		enreg.add(enregLabel);
		enreg.add(demarrer);
		enreg.add(stopper);
		enreg.add(rejouer);
		
		redoUndo.add(refaire);
		redoUndo.add(defaire);
		
		/* Ajout des listeners sur le JTextArea */
		txtArea.addCaretListener(new CaretListener(){
			@Override
			public void caretUpdate(CaretEvent e) {
				debutSelection= e.getDot();
				finSelection = e.getMark();

				exeCmd(CommandName.SELECT);
			}
		});
		
		txtArea.addKeyListener(new KeyListener(){
			@Override
			public void keyPressed(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {
				text = txtArea.getText();
				lastChar = e.getKeyChar();
				exeCmd(CommandName.INSERT);
				
				// Ajout de la sauvegarde du l'�tat actuel pour defaire/refaire
				text = txtArea.getText();
				exeCmd(CommandName.ADDBACKUP);
			}

			@Override
			public void keyTyped(KeyEvent e) {}
			
		});
		
		/* Ajout d'une barre de d�filement sur le JTextArea */
		JScrollPane scrollTxt = new JScrollPane(txtArea); 
		txtArea.setEditable(true);
		
		/* Disposition des differents panels de la fenetre */
		north.add(menu, BorderLayout.WEST);
		north.add(enreg, BorderLayout.CENTER);
		north.add(redoUndo, BorderLayout.EAST);
		container.add(north, BorderLayout.NORTH);
		container.add(scrollTxt, BorderLayout.CENTER);
	    this.setContentPane(container);
	    
	    /* Affichage de la fenetre */
		setVisible(true);

	}

	public void setCommandes(HashMap<CommandName, Command> commandes) {
		this.commands = commandes;
	}

	@Override
	public String getText() {
		return text;
	}
	
	@Override
	public void setText(String texte) {
		txtArea.setText(texte);
	}

	@Override
	public String getLastChar() {
		return String.valueOf(lastChar);
	}

	@Override
	public int getStartPosSelection() {
		return debutSelection;
	}

	@Override
	public int getEndPosSelection() {
		return finSelection;
	}

	@Override
	public int getTextLength(){
		return text.length();
	}

	/**
	 * Execute la commande passee en parametre
	 * @param cmd
	 */
	public void exeCmd(CommandName cmd) {
		commands.get(cmd).execute();
	}
	
	public void enableRecord(boolean bool) {
		demarrer.setEnabled(bool);
	}
	
	public void enableStop(boolean bool) {
		stopper.setEnabled(bool);
	}
	
	public void enableStart(boolean bool) {
		rejouer.setEnabled(bool);
	}
	
	@Override
	public void enableUndo(boolean bool) {
		defaire.setEnabled(bool);
	}

	@Override
	public void enableRedo(boolean bool) {
		refaire.setEnabled(bool);
	}
	
	private ImageIcon resize(ImageIcon i, int h, int w){
	   Image img = i.getImage() ;  
	   Image newimg = img.getScaledInstance( w,h,  java.awt.Image.SCALE_SMOOTH ) ;  
	   return new ImageIcon( newimg );
	}
}
