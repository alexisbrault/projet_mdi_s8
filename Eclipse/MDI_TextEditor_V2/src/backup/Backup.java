package backup;

import java.util.LinkedList;
import java.util.List;

import invoker.UI;
import memento.MementoBackup;
import receiver.MoteurEdition;

/**
 * Buffer class used to undo and redo commands *
 */
public class Backup {

	private MoteurEdition motor;
	private UI ui;

	private List<MementoBackup> versions;
	private int currentVersionIndex;

	public Backup(MoteurEdition motor, UI ui){
		this.motor = motor;
		this.ui = ui;

		this.versions = new LinkedList<MementoBackup>();
		this.currentVersionIndex = 0;
	}

	/**
	 * Add a version to the backup
	 * @param memento current
	 */
	public void add(MementoBackup memento){
		versions.add(memento);
		currentVersionIndex = versions.size()-1;
		ui.enableUndo(currentVersionIndex > 0);
	}

	/**
	 * Move back the version index
	 */
	public void undo(){
		if(currentVersionIndex > 0){
			currentVersionIndex--;
			setNewState();

		}
		enableAction();
	}

	/**
	 * Move up the version index
	 */
	public void redo(){
		if(currentVersionIndex < versions.size()-1){
			currentVersionIndex++;
			setNewState();
		}
		enableAction();
	}

	/**
	 * Update the actions in the UI
	 */
	private void enableAction(){
		ui.enableUndo(currentVersionIndex >0);
		ui.enableRedo(currentVersionIndex < versions.size()-1);
	}

	/**
	 * Update the buffer
	 */
	private void setNewState() {
		String newText = versions.get(currentVersionIndex).getContent(); //TODO : Change to an iterator loop
		ui.setText(newText);
		motor.setContenuBuffer(newText);
	}

	public UI getUi() {
		return ui;
	}

	public void setUi(UI ui) {
		this.ui = ui;
	}
}
