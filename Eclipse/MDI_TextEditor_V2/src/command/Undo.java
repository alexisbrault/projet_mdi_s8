package command;

import backup.Backup;
import memento.Memento;

/**
 * Undo last text edited
 *
 */
public class Undo implements Command{

	private Backup backup;
	
	public Undo(Backup backup){
		this.backup = backup;
	}
	
	@Override
	public void execute() {
		backup.undo();
	}

	@Override
	public void setMemento(Memento memento) {}

}
