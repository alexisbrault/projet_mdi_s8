package command;

public enum CommandName {
	CUT, PASTE, COPY, INSERT, SELECT, START, STOP, REPLAY, UNDO, REDO, ADDBACKUP
}
