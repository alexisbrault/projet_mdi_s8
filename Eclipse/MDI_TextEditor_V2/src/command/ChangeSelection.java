package command;

import invoker.UI;
import memento.Memento;
import memento.MementoSelection;
import receiver.MoteurEdition;
import recorder.MementoCommand;
import recorder.Recorder;

/**
 * Selection with the cursor
 *
 */
public class ChangeSelection implements Command{

	private MoteurEdition motor;
	private UI ui;
	private Recorder recorder;

	private Memento memento;

	public ChangeSelection(MoteurEdition m, UI ui, Recorder r) {
		this.motor = m;
		this.ui = ui;
		this.recorder = r;
	}

	@Override
	public void execute() {
		if(recorder.isReplaying()){
			if(memento instanceof MementoSelection)
				motor.changeSelection(( (MementoSelection) memento).getStartPos(), ((MementoSelection) memento).getEndPos());
			
		}else{
			motor.changeSelection(ui.getStartPosSelection(), ui.getEndPosSelection());

			if(motor.isRecording()){
				recorder.addCommandMemento(new MementoCommand(this, new MementoSelection(ui.getStartPosSelection(), 
						ui.getEndPosSelection())));
			}
		}
	}

	@Override
	public void setMemento(Memento memento) {
		this.memento = memento;
	}

}
