package command;

import memento.Memento;
import recorder.Recorder;

/**
 * Start recording
 *
 */
public class Start implements Command{

	private Recorder recorder;
	
	public Start(Recorder r) {
		recorder = r;
	}
	
	@Override
	public void execute() {
		recorder.record();
	}

	@Override
	public void setMemento(Memento memento) {}
}
