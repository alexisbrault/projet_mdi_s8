package command;

import invoker.UI;
import memento.Memento;
import memento.MementoInsert;
import receiver.MoteurEdition;
import recorder.MementoCommand;
import recorder.Recorder;

/**
 * Insert text
 *
 */
public class Insert implements Command {

	private MoteurEdition motor;
	private UI ui;
	private Recorder recorder;
	private Memento memento;

	public Insert(MoteurEdition m, UI ui, Recorder r) {
		this.motor = m;
		this.ui = ui;
		this.recorder = r;
	}

	@Override
	public void execute() {
		if(recorder.isReplaying()){
			System.out.println(( (MementoInsert) memento).getText());
			motor.insert(( (MementoInsert) memento).getText());				
		}else{

			motor.insert(ui.getText());

			if(motor.isRecording()){
				recorder.addCommandMemento(new MementoCommand(this, new MementoInsert(ui.getText())));
			}
		}
	}

	public void setMemento(Memento memento){
		this.memento = memento;
	}
}
