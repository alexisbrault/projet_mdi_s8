package command;

import memento.Memento;
import memento.MementoCut;
import receiver.MoteurEdition;
import recorder.MementoCommand;
import recorder.Recorder;

/**
 * Cut the selected text 
 *
 */
public class Cut implements Command {
	
	private MoteurEdition motor;
	private Recorder recorder;
	
	public Cut(MoteurEdition m, Recorder r) {
		this.motor = m;
		this.recorder = r;
	}
	
	@Override
	public void execute() {
		motor.cut();
		
		if(!recorder.isReplaying()){
			if(motor.isRecording()){
				recorder.addCommandMemento(new MementoCommand(this, new MementoCut()));
			}
		}
	}

	@Override
	public void setMemento(Memento memento) {}

}
