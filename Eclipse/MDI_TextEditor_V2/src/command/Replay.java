package command;

import memento.Memento;
import recorder.Recorder;

/**
 * Replay the macros
 *
 */
public class Replay implements Command{

	private Recorder recorder;
	
	public Replay(Recorder r) {
		recorder = r;
	}
	
	@Override
	public void execute() {
		recorder.replay();
	}

	@Override
	public void setMemento(Memento memento) {}
}
