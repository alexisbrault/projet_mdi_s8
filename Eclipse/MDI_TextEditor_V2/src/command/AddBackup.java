package command;

import backup.Backup;
import memento.Memento;
import memento.MementoBackup;

/**
 * Create a new state of backup
 *
 */
public class AddBackup implements Command{

	private Backup backup;
	
	public AddBackup(Backup backup){
		this.backup = backup;
		
		// Sauvegarde de l'�tat vide du buffer
		execute();
	}
	
	@Override
	public void execute() {
		backup.add(new MementoBackup(backup.getUi().getText()));
	}

	@Override
	public void setMemento(Memento memento) {}

}
