package command;

import memento.Memento;
import memento.MementoCopy;
import receiver.MoteurEdition;
import recorder.MementoCommand;
import recorder.Recorder;

/**
 * Copy the selected text
 *
 */
public class Copy implements Command {

	private MoteurEdition motor;
	private Recorder recorder;

	public Copy(MoteurEdition m, Recorder r) {
		this.motor = m;
		this.recorder = r;
	}

	@Override
	public void execute() {
		motor.copy();

		if(!recorder.isReplaying()){
			if(motor.isRecording()){
				recorder.addCommandMemento(new MementoCommand(this, new MementoCopy()));
			}
		}
	}

	@Override
	public void setMemento(Memento memento) {}
}
