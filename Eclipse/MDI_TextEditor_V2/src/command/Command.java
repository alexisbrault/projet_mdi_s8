package command;

import memento.Memento;

/**
 * Command interface
 *
 */
public interface Command {
	/**
	 * Execute the command
	 */
	public void execute();
	
	/**
	 * Allow to update the memento of the command for the macros
	 * @param memento
	 */
	public void setMemento(Memento memento);
}
