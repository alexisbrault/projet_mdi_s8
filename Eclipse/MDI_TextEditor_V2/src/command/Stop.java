package command;

import memento.Memento;
import recorder.Recorder;

/**
 * Stop recording
 *
 */
public class Stop implements Command{

	private Recorder recorder;
	
	public Stop(Recorder r) {
		recorder = r;
	}
	
	@Override
	public void execute() {
		recorder.stop();
	}

	@Override
	public void setMemento(Memento memento) {}
}

