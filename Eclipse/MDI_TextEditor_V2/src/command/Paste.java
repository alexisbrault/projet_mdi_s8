package command;

import memento.Memento;
import memento.MementoPaste;
import receiver.MoteurEdition;
import recorder.MementoCommand;
import recorder.Recorder;

/**
 * Paste the copied text
 *
 */
public class Paste implements Command {
	
	private MoteurEdition motor;
	private Recorder recorder;
	
	public Paste(MoteurEdition m, Recorder r) {
		this.motor = m;
		this.recorder = r;
	}
	
	@Override
	public void execute() {
		motor.paste();
		
		if(!recorder.isReplaying()){
			if(motor.isRecording()){
				recorder.addCommandMemento(new MementoCommand(this, new MementoPaste()));
			}
		}
	}

	@Override
	public void setMemento(Memento memento) {}

}
