package command;

import backup.Backup;
import memento.Memento;

/**
 * Get text which have been undo
 *
 */
public class Redo implements Command{

private Backup backup;
	
	public Redo(Backup backup){
		this.backup = backup;
	}
	
	@Override
	public void execute() {
		backup.redo();
	}

	@Override
	public void setMemento(Memento memento) {}
}
