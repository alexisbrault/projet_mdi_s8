package memento;

public class MementoInsert implements Memento{

	private String text;
	
	public MementoInsert(String text){
		this.setText(text);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
