package memento;

public class MementoSelection implements Memento{

	private int startPos;
	private int endPos;
	
	public MementoSelection(int startPos, int endPos){
		setStartPos(startPos);
		setEndPos(endPos);
	}

	public int getStartPos() {
		return startPos;
	}

	public void setStartPos(int debut) {
		this.startPos = debut;
	}

	public int getEndPos() {
		return endPos;
	}

	public void setEndPos(int endPos) {
		this.endPos = endPos;
	}
}
