package memento;

public class MementoBackup {

	private String content;
	
	public MementoBackup(String text){
		this.setContenu(text);
	}

	public String getContent() {
		return content;
	}

	public void setContenu(String content) {
		this.content = content;
	}

	@Override
	public boolean equals(Object obj) {
		return (content.equals(obj.toString()));
	}

	@Override
	public String toString() {
		return content;
	}
	
	
}
