package receiver;

public class Selection {
	private int startPos = 0;
	private int endPos = 0;
	
	public Selection(int startPos, int endPos) {		
		setPositions(startPos, endPos);
	}
	
	public int getStartPosition() {
		return startPos;
	}
	
	public int getEndPosition() {
		return endPos;
	}
	
	public int getLength() {
		return (this.endPos - this.startPos);
	}
	
	public void setPositions(int debut, int fin) {
		if(debut < 0){
			this.startPos = 0;
		}else{
			if(fin < debut){
				this.startPos = fin;
				this.endPos = debut;
			}else{
				this.startPos = debut;
				this.endPos = fin;
			}
		}
	}
}
