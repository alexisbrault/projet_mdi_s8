package receiver;

public class Buffer {
	private final int BASE_CAPACITY = 500;
	
	private StringBuffer value;
	
	public Buffer() {
		value = new StringBuffer(BASE_CAPACITY);
	}
	
	/**
	 * 
	 * @param start Index de debut de la lecture
	 * @param end Index de fin de la lecture 
	 * @param delete Indique s'il faut ou non conserver le texte lu 
	 * (true: supprimer, false: ne rien faire)
	 * @throws StringIndexOutOfBoundsException si le debut est negatif, superieur a la fin
	 *	ou superieur a length() 
	 * @return Le texte lu dans le buffer
	 */
	public String read(int start, int end, boolean delete) throws StringIndexOutOfBoundsException {
		if(value.length()>0){
			if(start > value.length()-1){
				start=value.length()-1;
			}
			if(end > value.length()-1){
				end=value.length()-1;
			}
			String subStr = value.substring(start, end);
			
			if(delete) value.delete(start, end);
			
			return subStr;
		}
		return null;
	}
	
	/**
	 * Insere un texte a un endroit donne en preservant le reste du texte deja ecrit
	 * @param start Index de debut de l'ecriture
	 * @param text Texte e ecrire e l'emplacement choisi
	 * @throws StringIndexOutOfBoundsException si le debut est negatif ou superieur e length() 				
	 */
	public void write(int start, String text) throws StringIndexOutOfBoundsException {
		if(value.length()>0){
			if(value.length()!=0){
				value.delete(0, value.length());
			}
			
			value.append(text);
		}
	}
	
	/**
	 * Insere un texte a un emplacement donne en supprimant le contenu precedemment ecrit
	 * @param start Index de debut de l'ecriture
	 * @param end Index de fin de l'ecriture
	 * @param text Texte e ecrire e l'emplacement choisi
	 * @throws StringIndexOutOfBoundsException si le debut est negatif, superieur a la fin
	 *	ou superieur a length() 					
	 */
	public void write(int start, int end, String text) throws StringIndexOutOfBoundsException {
		if(value.length()>0){
			if(start > value.length()-1){
				start=value.length()-1;
			}
			if(end > value.length()-1){
				end=value.length()-1;
			}
			value.replace(start, end, text);
		}
	}
	
	/**
	 * @return la longueur du contenu du buffer
	 */
	public int getLength(){
		return value.length();
	}

	/**
	 * Remplace tout le buffer par le texte passe en parametre
	 * @param text a mettre dans le buffer
	 */
	public void clear(String text){
		value.replace(0, value.length(), text);
	}
}
