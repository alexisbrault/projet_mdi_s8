package receiver;

public interface MoteurEdition {
	/**
	 * Commande couper
	 */
	public void cut();
	/**
	 * Commande copier
	 */
	public void copy();
	/**
	 * Commande coller
	 */
	public void paste();
	/**
	 * Commande d'insertion de texte
	 * @param s, texte a inserer
	 */
	public void insert(String s);
	/**
	 * Commande de gestion de la selection
	 * @param debut du texte a selectionner
	 * @param fin du texte a selectionner
	 */
	public void changeSelection(int debut, int fin);
	
	/**
	 * @return vrai si on enregistre les commandes pour la macro, faux sinon
	 */
	public boolean isRecording();
	/**
	 * Met a jour le booleen d'enregistrement des macros 
	 * @param recording
	 */
	public void setRecording(boolean recording);
	
	void setContenuBuffer(String text);
}