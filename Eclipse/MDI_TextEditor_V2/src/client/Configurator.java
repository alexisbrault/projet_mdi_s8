package client;

import java.io.IOException;
import java.util.HashMap;

import backup.Backup;
import command.AddBackup;
import command.ChangeSelection;
import command.Command;
import command.CommandName;
import command.Copy;
import command.Cut;
import command.Insert;
import command.Paste;
import command.Redo;
import command.Replay;
import command.Start;
import command.Stop;
import command.Undo;
import invoker.UI;
import invoker.UIImpl;
import receiver.MoteurEdition;
import receiver.MoteurEditionImpl;
import recorder.Recorder;
import recorder.RecorderImpl;

/**
 * Class contain the main
 * Client part with command pattern
 *
 */
public class Configurator {
	
	/**
	 * Objets de l'editeur
	 */
	private static UI ui;
	private static MoteurEdition motor;
	private static Recorder recorder;
	private static Backup backup;
	
	/**
	 * Instantiate tools editor
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		/* Insertion des commandes dans la liste des commandes */
		HashMap<CommandName, Command> commands = new HashMap<CommandName, Command>();
		
		ui = new UIImpl(commands);
		
		motor = new MoteurEditionImpl();
		backup = new Backup(motor, ui);
		recorder = new RecorderImpl(motor);
		
		
		commands.put(CommandName.CUT, new Cut(motor, recorder));
		commands.put(CommandName.PASTE, new Paste(motor, recorder));
		commands.put(CommandName.COPY, new Copy(motor, recorder));
		commands.put(CommandName.INSERT, new Insert(motor, ui, recorder));
		commands.put(CommandName.SELECT, new ChangeSelection(motor, ui, recorder));
		commands.put(CommandName.START, new Start(recorder));
		commands.put(CommandName.STOP, new Stop(recorder));
		commands.put(CommandName.REPLAY, new Replay(recorder));
		commands.put(CommandName.ADDBACKUP, new AddBackup(backup));
		commands.put(CommandName.UNDO, new Undo(backup));
		commands.put(CommandName.REDO, new Redo(backup));
	}
	
}
