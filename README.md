# README #

### Résumé du projet ###

Ce projet est effectué dans le cadre du cours MDI de l'ESIR 2ème année (Systèmes d'informations).
Le principe est de développer un éditeur de texte simple nécessitant une conception UML avancée.

### Membres du projet ###

* Alexis Brault
* Florent Catiau-Tristant
* Yoann Boyère
* Antoine-Adrien Parent